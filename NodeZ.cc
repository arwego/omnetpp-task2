#include <string.h>
#include <stdio.h>
#include <omnetpp.h>

using namespace omnetpp;

class NodeZ : public cSimpleModule
{
    private:
       int counterACKSent, bitLength, counterArrivedFromY;

    protected:
       virtual void generateNewMessage();
       virtual void initialize() override;
       virtual void handleMessage(cMessage *msg) override;
};

Define_Module(NodeZ);

void NodeZ::initialize()
{
    bitLength = par("bitLength");
    counterACKSent = 0;
    counterArrivedFromY = 0;
    WATCH(counterACKSent);
    WATCH(counterArrivedFromY);
}

void NodeZ::handleMessage(cMessage *msg)
{
    cPacket *arrive = check_and_cast<cPacket *>(msg);
    if (!(arrive->hasBitError())){
        counterArrivedFromY++;
        EV << "Message arrived from Y, send ACK to Y \n";
        generateNewMessage();
    } else {
        //in case error bit is true, delete the msg
        delete msg;
    }
}

void NodeZ::generateNewMessage()
{
    cPacket *pck = new cPacket("ACK");
    pck->setBitLength(bitLength);
    pck->setBitError(false);
    send(pck, "out");
    counterACKSent++;
 }
