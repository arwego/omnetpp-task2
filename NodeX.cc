#include <string.h>
#include <stdio.h>
#include <omnetpp.h>

using namespace omnetpp;

class NodeX : public cSimpleModule
{
    public:
        NodeX();
        virtual ~NodeX();

    private:
       cMessage *event;
       int counterTotalSent, bitLength;

    protected:
       virtual void generateNewMessage();
       virtual void initialize() override;
       virtual void handleMessage(cMessage *msg) override;
};

Define_Module(NodeX);

NodeX::NodeX()
{
    event = nullptr;
}

NodeX::~NodeX()
{
    cancelAndDelete(event);
}


void NodeX::initialize()
{
    bitLength = par("bitLength");
    event = new cMessage("event");
    counterTotalSent = 0;
    WATCH(counterTotalSent);

    EV << "Sending initial packet\n";
    generateNewMessage();
    scheduleAt(10.0, event);
}

void NodeX::handleMessage(cMessage *msg)
{
    if (msg == event) {
        EV << "Sending New Message to Y\n";
        cancelEvent(event);
        generateNewMessage();
        scheduleAt(simTime()+10.0, event);
    }
}

void NodeX::generateNewMessage()
{
    cPacket *pck = new cPacket("MSG");
    pck->setBitLength(bitLength);
    pck->setBitError(false);
    send(pck, "out");
    counterTotalSent++;
 }
