#include <string.h>
#include <stdio.h>
#include <omnetpp.h>

using namespace omnetpp;

class NodeY : public cSimpleModule
{
    private:
       int counterACKReceived, limitACK, counterForwardToZ;

    protected:
       virtual void initialize() override;
       virtual void handleMessage(cMessage *msg) override;
};

Define_Module(NodeY);

void NodeY::initialize()
{
    counterACKReceived = 0;
    counterForwardToZ = 0;
    limitACK = par("nACK");
    WATCH(counterACKReceived);
    WATCH(counterForwardToZ);
}

void NodeY::handleMessage(cMessage *msg)
{
    if (counterACKReceived == limitACK) {
        EV << "Limit ACK has been received \n";
        endSimulation();
    }

    cPacket *arrive = check_and_cast<cPacket *>(msg);
    if (!(arrive->hasBitError())){
        //Message from X
        if (strcmp("MSG", msg->getName())==0) {
            EV << "Message arrived from X, forward message to Z \n";
            send(msg, "out");
            counterForwardToZ++;
        //Message from Y
        } else if (strcmp("ACK", msg->getName())==0) {
            EV << "ACK arrived from Z \n";
            counterACKReceived++;
            delete msg;
        }
    } else {
        //in case error bit is true, delete the msg
        delete msg;
    }
}
