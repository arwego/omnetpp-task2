# Single Line Nodes #

Assume 3 nodes (X, Y, and Z) forming a line topology are ready for transmission.
The environments are in relatively extreme conditions, i.e. BER = 5.10^-3.
There are N packets available, with each packet has 1MB in length.
Node X is sending the packet to Node Y, one packet at a time, without the need of Node Y acknowledging back.
Then, Node Y will forward the packet to Node Z, and Node Z will acknowledge back.
Node Y will count and display the successfully received ACK from Node Z.

## Run Simulation with OMNeT++ 5.0 ##